<?php
namespace app\index\controller;

use app\common\model\Users;
use think\Controller;
use think\Db;
use think\Request;

class Index extends Controller
{
    public function index()
    {
        //$user = Users::find(1);
        //$user = Db::table('users')->select();
        //$user = \db('users')->select();
        return view('index');
        //return '<style type="text/css">*{ padding: 0; margin: 0; } .think_default_text{ padding: 4px 48px;} a{color:#2E5CD5;cursor: pointer;text-decoration: none} a:hover{text-decoration:underline; } body{ background: #fff; font-family: "Century Gothic","Microsoft yahei"; color: #333;font-size:18px} h1{ font-size: 100px; font-weight: normal; margin-bottom: 12px; } p{ line-height: 1.6em; font-size: 42px }</style><div style="padding: 24px 48px;"> <h1>:)</h1><p> ThinkPHP V5<br/><span style="font-size:30px">十年磨一剑 - 为API开发设计的高性能框架</span></p><span style="font-size:22px;">[ V5.0 版本由 <a href="http://www.qiniu.com" target="qiniu">七牛云</a> 独家赞助发布 ]</span></div><script type="text/javascript" src="https://tajs.qq.com/stats?sId=9347272" charset="UTF-8"></script><script type="text/javascript" src="https://e.topthink.com/Public/static/client.js"></script><think id="ad_bd568ce7058a1091"></think>';
    }

    public function sigfffffffffffnup1(Request $request)
    {
        $session = $request->session('username');
        if ($session){
            Db::table('users')->where('username','=',$session)->update(['have'=>1,'room'=>'学习室']);
            return $this->success('报加班成功','/tp/public/index/information/jiaban');
        }else{
            return "<script>alert('清先登陆');window.location.href='/tp/public/index/index/index'; </script>";
        }
        //return view();
    }

    public function signfffffffffffup2(Request $request)
    {
        $session = $request->session('username');
        if ($session){
            Db::table('users')->where('username','=',$session)->update(['have'=>1,'room'=>'助学室']);
            return $this->success('报加班成功','/tp/public/index/information/jiaban');
        }else{
            return "<script>alert('清先登陆');window.location.href='/tp/public/index/index/index'; </script>";
        }
        //return view();
    }

    public function sigfffffffffffffndown(Request $request)
    {
        $session = $request->session('username');
        if ($session){
            Db::table('users')->where('username','=',$session)->update(['have'=>0]);
            return $this->success('退报成功','/tp/public/index/information/jiaban');
        }else{
            return "<script>alert('清先登陆');window.location.href='/tp/public/index/index/index'; </script>";
        }
    }

    public function tefffffffffshu()
    {
        return view();
    }

    public function baoteffffffffffshu(Request $request)
    {
        $post = $request->post();
        $session = $request->session('username');
        $number = Db::table('users')->where('username','=',$session)->field('name')->find();
        if ($post['time']){
            $result = Db::table('teshu')->insert(['name'=>$number['name'],'content'=>$post['content'],'time'=>$post['time'],'with'=>$post['with'],'time'=>$post['time']]);
        }
        else{
            $result = Db::table('teshu')->insert(['name'=>$number['name'],'content'=>$post['content'],'time'=>$post['time'],'with'=>$post['with'],'time'=>'无']);
        }
        if($result){
            $this->success('特殊情况报名成功','/tp/public/index/Information/teshuinfo');
        }else {
            $this->error('报名失败，请联系管理员');
        }

    }

    public function finffffffffffish(Request $request)
    {
        $session = $request->session('username');
        $number = Db::table('users')->where('username','=',$session)->field('name')->find();
        $info = Db::table('teshu')->where('name','=',$number['name'])->where('finish','=',0)->order(['id'=>'desc'])->select();
        return view('finish',['info'=>$info]);
    }

    public function efffffffffffnd(Request $request)
    {
        $session = $request->session('username');
        $post = $request->post();
        if(isset($post['submit']))
        {
            $number = Db::table('users')->where('username','=',$session)->field('name')->find();
            $result = Db::table('teshu')->where('name','=',$number['name'])->where('content','=',$post['content'])->where('with','=',$post['with'])->update(['finish'=>1]);
        }
        else{
            return "<script>window.location.href='/tp/public/index/index/finish'; </script>";
        }
        if($result){
            $this->success('特殊情况退报成功','/tp/public/index/Information/teshuinfo');
        }else {
            $this->error('退报失败，请联系管理员');
        }
    }
}
