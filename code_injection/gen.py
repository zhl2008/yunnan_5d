#!/usr/bin/env python

import sys

res = {}
for i in range(0x20,0x7f):
    for j in range(0x20,0x7f):
        if i^j < 0x7f and i^j >= 0x20:
            #print chr(i),chr(j),chr(i^j)
            res[chr(i^j)] = chr(i) + chr(j)
print res

my_string = sys.argv[1]

final_res = ''
for char in my_string:
    if res.has_key(char):
        final_res += '("%s"^"%s").' %(res[char][0],res[char][1])
    else:
        exit('not found char: %s'%char)

print final_res[:-1]
