<?php
$mysql_conf = array(
    'host'    => '127.0.0.1', 
    'db'      => 'mysql', 
    'db_user' => 'root', 
    'db_pwd'  => '', 
    );
$pdo = new PDO("mysql:host=" . $mysql_conf['host'] . ";dbname=" . $mysql_conf['db'], $mysql_conf['db_user'], $mysql_conf['db_pwd']);
$pdo->exec("set names 'utf8'");
$sql = "select * from user where user = ? and host = ?";
$stmt = $pdo->prepare($sql);
$stmt->bindValue(1, 'root', PDO::PARAM_STR);
$stmt->bindValue(2, 'localhost', PDO::PARAM_STR);
	
$rs = $stmt->execute();
if ($rs) {
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
        var_dump($row);
    }
}

$pdo = null;
?>
