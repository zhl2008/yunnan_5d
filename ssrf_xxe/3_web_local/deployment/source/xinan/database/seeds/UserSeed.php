<?php

use think\migration\Seeder;

class UserSeed extends Seeder
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
        db('users')->insert(['username'=>'root','password'=>md5('root'),'priv'=>1]);
    }
}