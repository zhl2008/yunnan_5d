DROP DATABASE IF EXISTS security;
CREATE database security;
USE security;
CREATE TABLE users
		(
		id int(3) NOT NULL AUTO_INCREMENT,
		username varchar(500) NOT NULL,
		password varchar(20) NOT NULL,
		PRIMARY KEY (id)
		);
CREATE TABLE emails
		(
		id int(3) NOT NULL AUTO_INCREMENT,
		email varchar(30) NOT NULL,
		username varchar(20) NOT NULL,
		PRIMARY KEY (id)
		);
CREATE TABLE uagents
		(
		id int(3)NOT NULL AUTO_INCREMENT,
		uagent varchar(256) NOT NULL,
		ip_address varchar(35) NOT NULL,
		username varchar(20) NOT NULL,
		PRIMARY KEY (id)
		);
CREATE TABLE referers
		(
		id int(3)NOT NULL AUTO_INCREMENT,
		referer varchar(256) NOT NULL,
		ip_address varchar(35) NOT NULL,
		PRIMARY KEY (id)
		);

INSERT INTO security.users (id, username, password) VALUES ('1', 'Dumb', 'Dumb'), ('2', 'Angelina', 'I-kill-you'), ('3', 'Dummy', 'p@ssword'), ('4', 'secure', 'crappy'), ('5', 'stupid', 'stupidity'), ('6', 'superman', 'genious'), ('7', 'batman', 'mob!le'), ('8', 'admin', 'admin');

INSERT INTO `security`.`emails` (id,username,email) VALUES ('1','Dumb', 'Dumb@dhakkan.com'), ('2', 'Angelina', 'Angel@iloveu.com'), ('3','Dummy' ,'Dummy@dhakkan.local'), ('4','secure', 'secure@dhakkan.local'), ('5','stupid' ,'stupid@dhakkan.local'), ('6','superman', 'superman@dhakkan.local'), ('7','batman' ,'batman@dhakkan.local'), ('8','admin', 'admin@dhakkan.com');



