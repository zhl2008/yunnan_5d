#!/bin/sh

# copy flag
echo 'flag{378346c7ac53d624a8621efc745bbeab}' > /flag


# set the tomcat index
unzip /usr/local/tomcat/webapps/gallery.war -d /usr/local/tomcat/webapps/gallery 
rm -r ln /usr/local/tomcat/webapps/ROOT
ln -s /usr/local/tomcat/webapps/gallery /usr/local/tomcat/webapps/ROOT

# start tomcat
/usr/local/tomcat/bin/startup.sh

# demonize
/bin/bash
