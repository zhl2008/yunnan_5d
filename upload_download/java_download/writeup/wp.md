
# exploit analysis

1. 可以在index.html中发现flag is in /flag的提示，并且发现可疑页面:
/gallery/gallery.php?path=xxx

2. 猜测可能是SSRF,但是存在后缀过滤，只允许下载`jpg` `png`等图片格式的文件:

3. 又从网站的404页面可知该网站的后端程序其实是java，参考java的SSRF，完成如下构造：
`path=file:///flag%23.png`

4. 即可获得最终的flag








