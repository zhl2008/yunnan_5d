<?php

   function curl($u){
        $ch = curl_init();
        curl_setopt_array($ch, array(
        CURLOPT_SSL_VERIFYHOST => 0,
        CURLOPT_SSL_VERIFYPEER => 0,
        CURLOPT_HEADER => 0,
        CURLOPT_SSLVERSION => 4,
        CURLOPT_POST => 1,
        CURLOPT_POSTFIELDS => '',
        CURLOPT_FOLLOWLOCATION => 1,
	CURLOPT_REDIR_PROTOCOLS => CURLPROTO_ALL,
        CURLOPT_VERBOSE => 1
        ));
        curl_setopt($ch, CURLOPT_URL, $u);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_NOBODY, 0);
        $output = curl_exec($ch);
        curl_close($ch);
        return $output;
    }
if(isset($_REQUEST[url])){
    echo curl($_REQUEST[url]);
}else{
    highlight_file(__FILE__);
}
?>
