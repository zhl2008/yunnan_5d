#!/usr/bin/env python
from lib.core.enums import PRIORITY
import base64


__priority__ = PRIORITY.LOWEST

def dependencies():
    pass


def reverse(string):
    tmp = ['']*len(string)
    for i in range(len(string)):
        tmp[i] = string[len(string)-1-i]
    tmp = ''.join(tmp)
    print tmp 
    return tmp

def my_encode(msg):
    #print msg
    msg = msg.replace(" ",'4e'.decode('hex'))
    return msg

def my_encode_2(msg):
    return base64.b64encode(msg)
            
def tamper(payload, **kwargs):
    return my_encode_2(payload)
